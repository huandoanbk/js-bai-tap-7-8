/**
 * Bài 7-8
 */
function getID(IDname) {
  return document.getElementById(IDname);
}
var numArray = [];

var ngauNhien = false;
// Nếu chọn Thêm số ngẫu nhiên thì hiển thị box nhập số lượng số ngẫu nhiên muốn thêm và disable khung nhập số n và ngược lại
function themNgaunhien() {
  ngauNhien = getID("isNgaunhien").checked;
  if (ngauNhien) {
    getID("soluong").classList.remove("d-none");
    getID("soLuongNN").focus();
    getID("soN").classList.add("disabled");
  } else {
    getID("soluong").classList.add("d-none");
    getID("soN").classList.remove("disabled");
    getID("soN").focus();
  }
}

//hàm thêm x số vào mảng M
function addRandom(x, M) {
  for (var i = 1; i <= x; i++) {
    var plusOrminus = Math.random() < 0.5 ? -1 : 1;
    var a = plusOrminus * Math.floor(Math.random() * 1000);
    M.push(a);
  }
}

function themSo() {
  if (ngauNhien) {
    //nếu chọn thêm ngâu nhiên
    var x = getID("soLuongNN").value * 1;
    if (x < 0) {
      alert("Vui lòng nhập số lượng >0");
    } else {
      addRandom(x, numArray);
    }
  } //nếu chọn add từng số
  else {
    var n = getID("soN").value * 1;
    numArray.push(n);
  }
  getID("mangSo").innerText = numArray;
}

function xoaMang() {
  numArray = [];
  getID("mangSo").innerText = numArray;
}

function xoaDau() {
  numArray.shift();
  getID("mangSo").innerText = numArray;
}

function xoaCuoi() {
  numArray.pop();
  getID("mangSo").innerText = numArray;
}
// Bài 01 - Tổng số dương
function tinhTongduong() {
  for (var n = 0, r = 0; r < numArray.length; r++)
    numArray[r] > 0 && (n += numArray[r]);
  getID("tong__duong").innerHTML =
    "Tổng số dương: " + new Intl.NumberFormat().format(n);
}

//Bài 02 - Đếm số dương
function demSoduong() {
  for (var n = 0, r = 0; r < numArray.length; r++) numArray[r] > 0 && n++;
  getID("soluong__duong").innerHTML =
    "Số số dương: " + new Intl.NumberFormat().format(n);
}

//Bài 03 - Tìm số nhỏ nhất
function soMin() {
  for (var n = numArray[0], r = 1; r < numArray.length; r++)
    numArray[r] < n && (n = numArray[r]);
  getID("so__min").innerHTML = "Số nhỏ nhất: " + n;
}

//Bài 04 - Tìm số dương nhỏ nhất
function soduongMin() {
  for (var n = [], r = 0; r < numArray.length; r++)
    numArray[r] > 0 && n.push(numArray[r]);
  if (n.length > 0) {
    for (var e = n[0], r = 1; r < n.length; r++) n[r] < e && (e = n[r]);
    getID("soduong__min").innerHTML = "Số dương nhỏ nhất: " + e;
  } else getID("soduong__min").innerHTML = "Không có số dương trong mảng";
}

//Bài 05 - Tìm số chẵn cuối cùng
function soChancuoi() {
  for (var n = 0, r = 0; r < numArray.length; r++)
    numArray[r] % 2 == 0 && (n = numArray[r]);
  getID("sochan__cuoi").innerHTML = "Số chẵn cuối cùng: " + n;
}
//Bài 06 - Đổi chỗ
// Hàm phụ đổi chỗ 2 vị trí x,y của mảng A
function swap(x, y, arrayA) {
  var e = arrayA[x];
  (arrayA[x] = arrayA[y]), (arrayA[y] = e);
}

function doiCho() {
  swap(getID("viTri1").value, getID("viTri2").value, numArray);
  getID("mang__doicho").innerHTML = "Mảng sau khi đổi: " + numArray;
}

//Bài 07 - Sắp xếp tăng dần
function sortTang() {
  numArray.sort(function (a, b) {
    return a - b;
  });
  getID("mang__tang").innerHTML = "Mảng sau khi sắp xếp: " + numArray;
}

//Bài 08 - Tìm số nguyên tố
function isNguyento(n) {
  if (n < 2) return !1;
  for (var r = 2; r <= Math.sqrt(n); r++) if (n % r == 0) return !1;
  return !0;
}
function soNguyento() {
  for (var n = -1, r = 0; r < numArray.length; r++) {
    if (isNguyento(numArray[r])) {
      n = numArray[r];
      break;
    }
  }
  getID("so__nguyento").innerHTML = "Số nguyên tố đầu tiên: " + n;
}

//Bài 09 - Đếm số nguyên của mảng mới
var numArray2 = [];
var ngauNhien2 = false;

function themNgaunhien2() {
  ngauNhien2 = getID("isNgaunhien2").checked;
  if (ngauNhien2) {
    getID("soluong2").classList.remove("d-none");
    getID("soLuongNN2").focus();
    getID("soN2").classList.add("disabled");
  } else {
    getID("soluong2").classList.add("d-none");
    getID("soN2").classList.remove("disabled");
    getID("soN2").focus();
  }
}

function themSo2() {
  if (ngauNhien2) {
    //nếu chọn thêm ngâu nhiên
    var x = getID("soLuongNN2").value * 1;
    if (x < 0) {
      alert("Vui lòng nhập số lượng >0");
    } else {
      addRandom(x, numArray2);
    }
  } //nếu chọn add từng số
  else {
    var n = getID("soN2").value * 1;
    numArray2.push(n);
  }
  getID("mangSo2").innerText = numArray2;
}

function xoaMang2() {
  numArray2 = [];
  getID("mangSo2").innerText = numArray2;
}

function xoaDau2() {
  numArray2.shift();
  getID("mangSo2").innerText = numArray2;
}

function xoaCuoi2() {
  numArray2.pop();
  getID("mangSo2").innerText = numArray2;
}
function demSonguyen() {
  for (var n = 0, r = 0; r < numArray2.length; r++)
    Number.isInteger(numArray2[r]) && n++;
  getID("so__songuyen").innerHTML = "Số nguyên: " + n;
}

//Bài 10 - So sánh số lượng số âm và số dương
function soAmduong() {
  for (var n = 0, r = 0, e = 0; e < numArray.length; e++)
    numArray[e] > 0 ? n++ : numArray[e] < 0 && r++;
  getID("so__sanh").innerHTML =
    n > r
      ? "Số dương > Số âm"
      : n < r
      ? "Số âm > Số dương"
      : "Số âm = Số dương";
}
